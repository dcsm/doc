Related communications
======================

Presentations
-------------

* `2023.01.20 - DCSM Foundations <https://gitlab.com/dcsm/doc/-/raw/main/communication/2023.01.31_DCSM_Foundations.pdf>`_
* `2023.12.19 - Curate and publish your study with Solidipes <https://gitlab.com/dcsm/doc/-/raw/main/communication/2023.12.19_Curate_and_plublish_your_study_with_Solidipes.pdf>`_
* `2024.04.11 - On the necessity of curation for datasets to achieve FAIR standard goals in scientific publications (even in mechanics) <https://gitlab.com/dcsm/doc/-/raw/main/communication/2024.04.11_mega.pdf>`_
* `2024.05.29 - Data curation procedure at JTCAM On the usage of Solidipes and Jupyter Notebooks <https://gitlab.com/dcsm/doc/-/raw/main/communication/2024.05.29_gt_notebook.pdf>`_
* `2024.06.05 - An Open Source Project for Dissemination of Computational Solid Mechanics (DCSM) <https://gitlab.com/dcsm/doc/-/raw/main/communication/2024.06.05_ECCOMAS_2024.pdf>`_
* `2024.09.02 - JTCAM: a Diamond Open Access journal with datasets curation to achieve FAIR standards in scientific publications <https://gitlab.com/dcsm/doc/-/raw/main/communication/2024.09.02_openscience_days.pdf>`_
