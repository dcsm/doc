Introduction
============

Project description
-------------------

The purpose of the DCSM project is to help scientists **produce**, **share**, and **access** data, especially in the field of
computational solid mechanics. The project is funded by a Swiss Open Research Data (ORD) Grant delivered by
swissuniversities.

The realization of the DCSM project is embodied in the development of an Open Source software:
`Solidipes <https://solidipes.readthedocs.io/en/latest/>`_. It also builds upon the platform
`Renku <https://renku.readthedocs.io/>`_, developed by the Swiss Data Science Center.

The tools developed in the DCSM project must aid the scientists during the whole **Life Cycle** of their research project.
The life cycle of a research project in regard to data management can be divided into three main phases:

* **Conception**: description of data
* **Development**: production, analysis of data, typically involving collaboration
* **Publication**: cleaning, archiving, and sharing data

.. image:: images/lifecycle.svg

There are many ways to deal with data management at each phase of the life cycle. The main goal of the DCSM project is
to provide a tool that can guide the scientists in their choices and assist them throughout the whole project's life cycle.
Furthermore, beyond the publication step, the DCSM project aims at simplifying the process of **retrieving** an already
published project and reusing it as a starting point for reproducing or extending the research.
`Solidipes <https://solidipes.readthedocs.io/en/latest/>`_ is the central tool that is developed to fulfill all of
these goals.
