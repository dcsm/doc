Welcome to the DCSM documentation!
==================================

This part is a collection of documents assembled
to support the idea of data sharing, an essential piece
of the DCSM (Dissemination of Computational Solid Mechanics) project.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   introduction.rst
   data_publication/ouvrir_la_science.md
   data_publication/storage_speed_analysis.rst
   Related communications <communication>
