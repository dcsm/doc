# Configuration file for the Sphinx documentation builder.

# -- Project information

project = "DCSM"
copyright = "2023 EPFL (École Polytechnique Fédérale de Lausanne)"
author = "Son Pham-Ba, Guillaume Anciaux"

release = "0.1"
version = "0.1.0"

# -- General configuration

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.extlinks",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    'myst_parser'
]


templates_path = ["_templates"]

# -- Options for HTML output

html_theme = "sphinx_rtd_theme"

# -- Options for EPUB output
epub_show_urls = "footnote"
