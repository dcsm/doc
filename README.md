# Documentation

Documentation for the DCSM project (Dissemination of Computational Solid Mechanics).

See it on [Read the Docs](https://dcsm.readthedocs.io/en/latest/).


## Build

### Linux

```
pip install -r requirements.txt
make [target]
```
where `[target]` is the output format of your choice (for example `html`).

